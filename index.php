<?php

/* 
 * Copyright (C) 2017 docwho
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Having a separation between the presentation part of our application
 * and the "business logic" part is a good idea.
 */
// Presentation part of our applications
require_once( __DIR__ . '/template.php' );
// Business logic
require_once( __DIR__ . '/logic.php' );
// Configure
require_once( __DIR__ . '/config.inc.php' );

get_head();

/*
 * We are trying to avoid a proliferation of pages that each handle different
 * steps in a transaction
 *
 * Here's the logic that decides what to do based on request method
 */
/* Use the $_SERVER['REQUEST_METHOD'] variable to determine wheter the request
 * was submitted with the get or post method. If the get method was used without
 * query string, display the homepage.
 */
if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
  include __DIR__ . '/backend_get.php';
	// If GET method was used, print the form and play the card.
	method_get();
} else {
	include __DIR__ . '/backend_post.php';
	// If POST method was used, print & process the form.
	method_post();
}

get_footer();

exit();