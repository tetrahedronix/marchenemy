<?php
/**
 * 
 * @global type $source
 * @param type $deck Variable from HTTP query string.
 */
function first_card( $deck = '' ) {
	global $source;
	global $prefix;
	
	// Remove all illegal characters from $source
	$url = filter_var( $source, FILTER_SANITIZE_URL );

	if ( filter_var( $url, FILTER_VALIDATE_URL ) === false ) {
		$scheme = get_card_name( $deck, 1 );
		echo <<<CARD
<div class="scheme">
<a href="index.php?card=1&select=$deck">
	<img src="images/schemeback.jpg">
	<span class="cardname">$scheme</span></a></div>
CARD
. PHP_EOL;
	} else {
		$scheme = get_card_image( $deck, 1 );
		echo<<<CARD
<div class="scheme">
<a href="index.php?card=1&select=$deck">
	<img src="$url/$prefix$scheme"></a></div>
CARD
. PHP_EOL;

	}
}

function get_card_image( $deck = '', $card = 1 ) {
	
	if ( $card > 0 ) {
		$card--;
	}
	
	$scheme = get_scheme( $deck, $card );
	$img = array_values( $scheme );
	
	return $img[0][1]; 
}

function get_card_name( $deck = '', $card = 1 ) {
	
	if ( $card > 0 ) {
		$card--;
	}

	$scheme = get_scheme( $deck, $card );
	 
	return key( $scheme );
}

/**
 * Data structure for Card-frequency-image relationship
 * 
 * @param type $deck Variable from HTTP query string
 * @return array card-frequency-image data structure
 */
function get_decklist( $deck = '' ) {
	$scheme_list = array ();
	
	switch ( $deck ) {
		case 'atdm_decklist':
			$scheme_list = array ( 
				'All in Good Time' => array ( 
					1, 'allingoodtime.jpg' ),
				'Behold the Power of Destruction' => array ( 
					1, 'beholdthepowerofdestruction.jpg' ),
				'Embrace My Diabolical Vision' => array (
					1, 'embracemydiabolicalvision.jpg'),
				'Feed the Machine' => array( 
					2, 'feedthemachine.jpg' ),
				'I delight in Your Convulsions' => array (
					1, 'idelightinyourconvulsions.jpg' ),
				'Ignite The Cloneforge!' => array (
					2, 'ignitethecloneforge.jpg' ),
				'Introductions Are in Order' => array (
					1, 'introductionsareinorder.jpg' ),
				'The Iron Guardian Stirs' => array (
					2, 'theironguardianstirs.jpg' ),
				'My Genius Knows No Bounds' => array (
					1, 'mygeniusknowsnobounds.jpg' ),
				'Realms Befitting My Majesty' => array (
					1, 'realmsbefittingmymajesty.jpg' ),
				'The Pieces Are Coming Together' => array (
					2, 'thepiecesarecomingtogether.jpg' ),
				'Your Fate Is Thrice Sealed' => array(
					1, 'yourfateisthricesealed.jpg' ),
				'Your Puny Minds Cannot Fathom' => array (
					1, 'yourpunymindscannotfathom.jpg' ),
				'I know All I See All' => array (
					2, 'iknowalliseeall.jpg' ),
				'Nothing Can Stop Me Now' => array (
					1, 'nothingcanstopmenow.jpg' ),
			);
			break;
		case 'batua_decklist':
			$scheme_list = array (
				'Choose Your Champion' => array (
					2, 'chooseyourchampion.jpg' ),
				'Dance, Pathetic Marionette' => array (
					1, 'dancepatheticmarionette.jpg' ),
				'The Dead Shall Serve' => array (
					2, 'thedeadshallserve.jpg'),
				'A Display of My Dark Power' => array (
					1, 'adisplayofmydarkpower.jpg' ),
				'Every Hope Shall Vanish' => array (
					2, 'everyhopeshallvanish.jpg' ),
				'I Delight in Your Convulsions' => array (
					1, 'idelightinyourconvulsions.jpg' ),
				'Introductions Are in Order' => array (
					1, 'introductionsareinorder.jpg' ),
				'Mortal Flesh Is Weak' => array (
					1, 'mortalfleshisweak.jpg' ),
				'Only Blood Ends Your Nightmares' => array (
					2, 'onlybloodendsyournightmares.jpg' ),
				'Realms Befitting My Majesty' => array ( 1,
					'realmsbefittingmymajesty.jpg' ),
				'Rotted Ones, Lay Siege' => array (
					2, 'rottedoneslaysiege.jpg'),
				'Surrender Your Thoughts' => array (
					1, 'surrenderyourthoughts.jpg' ),
				'Your Fate Is Thrice Sealed' => array (
					1, 'yourfateisthricesealed.jpg' ),
				'Your Puny Minds Cannot Fathom' => array ( 
					1, 'yourpunymindscannotfathom.jpg' ),
				'My Undead Horde Awakens' => array (
					1, 'myundeadhordeawakens.jpg' )
			);
			break;
		case 'stwwd_decklist':
			$scheme_list = array (
				'All Shall Smolder in My Wake' => array( 
					2, 'allshallsmolderinmywake.jpg' ),
				'Approach My Molten Realm' => array ( 
					1, 'approachmymoltenrealm.jpg' ),
				'I Delight in Your Convulsions' => array (
					1, 'idelightinyourconvulsions.jpg' ),
				'Introductions Are in Order' => array (
					1, 'introductionsareinorder.jpg' ),
				'Know Naught but Fire' => array (
					1, 'knownaughtbutfire.jpg' ),
				'Look Skyward and Despair' => array (
					2, 'lookskywardanddespair.jpg' ),
				'My Crushing Masterstroke' => array (
					1, 'mycrushingmasterstroke.jpg' ),
				'My Wish Is Your Command' => array (
					2, 'mywishisyourcommand.jpg' ),
				'Realms Befitting My Majesty' => array (
					1, 'realmsbefittingmymajesty.jpg' ),
				'The Fate of the Flammable' => array (
					1, 'thefateoftheflammable.jpg' ),
				'Tooth, Claw, and Tail' => array (
					1, 'toothclawandtail.jpg' ),
				'Which of You Burns Brightest?' => array (
					2, 'whichofyouburnsbrightest.jpg' ),
				'Your Fate Is Thrice Sealed' => array (
					1, 'yourfateisthricesealed.jpg' ),
				'Your Puny Minds Cannot Fathom' => array (
					1, 'yourpunymindscannotfathom.jpg' ),
				'I Bask in Your Silent Awe' => array (
					2, 'ibaskinyoursilentawe.jpg' )
			);
			break;
		case 'tcu_decklist':
			$scheme_list = array (
				'Every Last Vestige Shall Rot' => array( 
					2, 'everylastvestigeshallrot.jpg' ),
				'Evil Comes to Fruition' => array (
					1, 'evilcomestofruition.jpg' ),
				'I Call on the Ancient Magics' => array (
					1, 'icallontheancientmagics.jpg' ),
				'I Delight in Your Convulsions' => array (
					1, 'idelightinyourconvulsions.jpg' ),
				'Into the Earthen Maw' => array (
					1, 'intotheearthenmaw.jpg' ),
				'Introductions Are in Order' => array (
					1, 'introductionsareinorder.jpg' ),
				'May Civilization Collapse' => array (
					1, 'maycivilizationcollapse.jpg' ),
				'Nature Demands an Offering' => array (
					2, 'naturedemandsanoffering.jpg' ),
				'Realms Befitting My Majesty' => array (
					1, 'realmsbefittingmymajesty.jpg' ),
				'Roots of All Evil' => array (
					2, 'rootsofallevil.jpg' ),
				'Your Fate Is Thrice Sealed' => array (
					1, 'yourfateisthricesealed.jpg' ),
				'Your Puny Minds Cannot Fathom' => array (
					1, 'yourpunymindscannotfathom.jpg' ),
				'Your Will Is Not Your Own' => array (
					2, 'yourwillisnotyourown.jpg' ),
				'Nature Shields Its Own' => array (
					2, 'natureshieldsitsown.jpg' ),
				'The Very Soil Shall Shake' => array (
					1, 'theverysoilshallshake.jpg' ) 
			);
			break;
	}
	
	return $scheme_list;
}

function get_scheme( $deck = '', $card = 1 ) {
	
	if ( '' === $deck ) {
		return;
	}
	
	$schemes = unserialize( file_get_contents( $deck.'.deck' ) ); 
	$drawn = $schemes[$card];  // Ex. 18
	$decklist = get_decklist( $deck );
	$cur = 0;

	foreach( $decklist as $key => $value ) {
		$cur += $value[0];

		if ( $drawn == $cur ) {
			return array ( $key => $value );
		}

		if ( $drawn == ( $cur - 1 ) ) {
			return array ( $key => $value );
		}
	} 

}

function get_selected_deck( $select = '' ) {
	$choice = 0;
	
	switch ( $select ) {
		case 'atdm_decklist':
			$choice = 1;
			break;
		case 'batua_decklist':
			$choice = 2;
			break;
	  case 'stwwd_decklist':
			$choice = 3;
			break;
	  case 'tcu_decklist':
			$choice = 4;
			break;
		default:
			$choice = 0;
	}

	return $choice;
}

function play( $deck = '', $card = 0 ) {
	global $source;
	global $prefix;
	
	$schemes = get_decklist( $deck );
	$tot = 0;
	
	foreach ( $schemes as $freq) {
		$tot += $freq[0];
	}

	$card++;

	if ( $card > $tot ) {
		get_cover();
		return $card;
	}

	// Remove all illegal characters from $source
	$url = filter_var( $source, FILTER_SANITIZE_URL );

	if ( filter_var( $url, FILTER_VALIDATE_URL ) === false ) {
		$card_name = get_card_name( $deck, $card );
		echo<<<CARD
<div class="scheme">
<a href="index.php?card=$card&select=$deck">
	<img src="images/schemeback.jpg">
	<span class="cardname">$card_name</span></a></div>
CARD
. PHP_EOL;
	} else {
		$img = get_card_image( $deck, $card );
		echo<<<CARD
<div class="scheme">
<a href="index.php?card=$card&select=$deck">
	<img src="$url/$prefix$img"></a></div>
CARD
. PHP_EOL;

	}
}

function setup_deck( $deck = '' ) {
	global $msg;
	
	$schemes = get_decklist( $deck );
	
	if ( empty( $schemes ) ) {
		$msg[] = 'Can not shuffle the deck';
		return null;
	} else {
		$msg[] = 'Shuffling... OK!';
	}

	$tot = 0;
	foreach ( $schemes as $freq) {
		$tot += $freq[0];
	}
	
	$gs = shuffle_deck( $tot );
	file_put_contents( $deck.".deck", serialize( $gs ) );
}

function shuffle_deck( $roll = 1 ) {

	$cards = range( 1, $roll );
	shuffle( $cards );

	return $cards;
}
